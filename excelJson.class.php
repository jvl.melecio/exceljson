<?php

/**
 * @author: José Refugio Melecio Soto
 * @version: 1.0 2019-12-28
 * @description: Clase que lee un excel, devuelve la inforacion en un json y vice versa.
 */

class excelJson{

    private $m_estructuraExcel;
    private $m_configuracion;
    private $m_datos;
    private $o_exel;

    public function __construct($m_estructuraExcel,$m_configuracion){
        require_once("includes/phpExcel.php");
        $this->m_estructuraExcel = json_decode($m_estructuraExcel,1);
        $this->m_configuracion   = json_decode($m_configuracion,1);
        $this->m_datos = array("status" => array("status"  => "",
                                                  "mensaje" => array()),
                                "excel" => array()
                                );
        if($this->m_configuracion['accion'] == "lectura")
            $this->o_excel = "";
        else
            $this->o_excel = new PHPExcel();
    }

    private function propiedades(){
        $this->o_excel->getProperties()->setCreator($m_configuracion['autor'])
                                        ->setLastModifiedBy($m_configuracion['autor'])
                                        ->setTitle("Plantilla para importacion")
                                        ->setSubject("Plantilla para importacion")
                                        ->setDescription("Plantilla generada para la importacion de {$m_configuracion['nombre']}");
    }

}

?>